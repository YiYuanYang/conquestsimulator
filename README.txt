The simulator runs every combination of 4 decks from lineups1.csv against the field(lineups2.csv). 
The winrates of every specific deck is stored in matchups.csv.

winrates.csv is a matrix of winrates between decks. Every deck is identified by their index on winrates.csv starting from 0.
lineups1.csv should contain at least 4 integers. 
lineups2.csv should contain at least one row of 4 integers. Every row must contain only 4 integers. 

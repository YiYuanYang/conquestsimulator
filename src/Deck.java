public class Deck {
	
	private boolean won;
	private int deckID;

	Deck(int id) {
		won = false;
		deckID = id;
	}
	
	public int getDeckID() {
		return deckID;
	}

	public boolean isWon() {
		return won;
	}

	public void setWon(boolean won) {
		this.won = won;
	}

}

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Simulator {
	
	private double[][] winrates;
	private int[] decks1;
	private Lineup[] lineups1;
	private Lineup[] lineups2;
	
	Simulator() throws FileNotFoundException, IOException {
		//Import winrates from csv
		List<List<String>> records = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader("winrates.csv"))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		        String[] values = line.split(",");
		        records.add(Arrays.asList(values));
		    }
		}
		double[][] wrMatrix = new double[records.size()][records.size()];
		for(int i=0; i<wrMatrix.length; i++) {
			for(int j=0; j<wrMatrix[0].length; j++) {
				wrMatrix[i][j] = Double.parseDouble(records.get(i).get(j));
			}
		}
		winrates = wrMatrix;
		
		//Import field of lineup2's from csv
		records.clear();
		try (BufferedReader br = new BufferedReader(new FileReader("lineups2.csv"))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		        String[] values = line.split(",");
		        records.add(Arrays.asList(values));
		    }
		}
		int[][] lineup2Decks = new int[records.size()][records.get(0).size()];
		for(int i=0; i<lineup2Decks.length; i++) {
			for(int j=0; j<lineup2Decks[0].length; j++) {
				lineup2Decks[i][j] = Integer.parseInt(records.get(i).get(j));
			}
		}
		Lineup[] temp = new Lineup[lineup2Decks.length];
		for(int i=0; i<lineup2Decks.length; i++) {
			Deck[] sample = {new Deck(lineup2Decks[i][0]), new Deck(lineup2Decks[i][1]), new Deck(lineup2Decks[i][2]), new Deck(lineup2Decks[i][3])};
			temp[i] = new Lineup(sample);
		}
		lineups2 = temp;
		
		//Import fields of lineup1's from csv
		String[] values = {};
		try (BufferedReader br = new BufferedReader(new FileReader("lineups1.csv"))) {
		    String line;
		    line = br.readLine();
		    values = line.split(",");
		}
		int[] temp1 = new int[values.length];
		for(int i=0; i<values.length; i++) {
			temp1[i] = Integer.parseInt(values[i]);
		}
		decks1 = temp1;
	}

	/*
	 * Calls SeriesSimulator to find the wr of a lineup against a field
	 */
	public double verseLineups(Lineup lineup1) {
		double sum = 0;
		for(int i=0; i<lineups2.length; i++) {
			SeriesSimulator seriesSimulation = new SeriesSimulator(this.getWinrates(), lineup1, lineups2[i]);
			sum += seriesSimulation.verse();
		}
		return sum/(lineups2.length);
	}
	
	//Downloaded method. returns all combinations from 1 to n.
	public ArrayList<ArrayList<Integer>> combine(int n, int k) {
		ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
	 
		if (n <= 0 || n < k)
			return result;
	 
		ArrayList<Integer> item = new ArrayList<Integer>();
		dfs(n, k, 1, item, result); // because it need to begin from 1
	 
		return result;
	}
	 
	//Downloaded helper method for combine
	private void dfs(int n, int k, int start, ArrayList<Integer> item,
			ArrayList<ArrayList<Integer>> res) {
		if (item.size() == k) {
			res.add(new ArrayList<Integer>(item));
			return;
		}
	 
		for (int i = start; i <= n; i++) {
			item.add(i);
			dfs(n, k, i + 1, item, res);
			item.remove(item.size() - 1);
		}
	}
	
	/*
	 * Finds the best 4 combinations of decks against a lineup.
	 */
	public void lineupFinder() {
		//make all combinations of lineups
		ArrayList<ArrayList<Integer>> combination = combine(decks1.length, 4);
		for(int i=0; i<combination.size(); i++) {
			for(int j=0; j<4; j++) {
				combination.get(i).set(j, combination.get(i).get(j) - 1);
			}
		}
		
		lineups1 = new Lineup[combination.size()];
		for(int i=0; i<lineups1.length; i++) {
			Deck[] decks = new Deck[4];
			for(int j=0; j<4; j++) {
				decks[j] = new Deck(decks1[combination.get(i).get(j)]);
			}
			lineups1[i] = new Lineup(decks);
		}
		
		//use all lineups to verse field
		for(Lineup element : lineups1) {
			element.setWrAgainstField(verseLineups(element));
		}
		
		//put winrate and lineup into Arraylist to sort
		Arrays.sort(lineups1, new SortByWR());
		
		//print
		int numberOfTopDecks = lineups1.length;
		for(int i=0; i<numberOfTopDecks; i++) {
			for(Deck element: lineups1[i].getDecks()) {
				System.out.print(element.getDeckID() + ",");
			}
			System.out.println(lineups1[i].getWrAgainstField());
		}
	}
	
	class SortByWR implements Comparator<Lineup> {
	    public int compare(Lineup a, Lineup b) {
	        if ( a.getWrAgainstField() > b.getWrAgainstField() ) return -1;
	        else if ( a.getWrAgainstField() == b.getWrAgainstField() ) return 0;
	        else return 1;
	    }
	}
	
	public double[][] getWinrates() {
		return winrates;
	}

	public static void main(String[] args) throws FileNotFoundException, IOException {
		Simulator simulation = new Simulator();
		//Deck[] decks = {new Deck(20), new Deck(26), new Deck(3), new Deck(34)};
		//System.out.print(simulation.verseLineups(new Lineup(decks)));
		simulation.lineupFinder();
	}

}

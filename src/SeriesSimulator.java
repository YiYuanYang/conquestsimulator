/*
 * This class simulates a BO5 series between two lineups
 */
public class SeriesSimulator {
	
	private double[][] winrates;
	private final static int sampleSize = 100000;
	private static boolean[] wins;
	private Lineup lineup1;
	private Lineup lineup2;
	private int seriesNumber;
	
	SeriesSimulator(double[][] winrates, Lineup lineup1, Lineup lineup2) {
		this.winrates = winrates;
		this.lineup1 = lineup1;
		this.lineup2 = lineup2;
		wins = new boolean[sampleSize];
		seriesNumber = 0;
		banDecks();
	}
	
	/*
	 * Returns winrate for lineup1 against lineup2.
	 */
	public double verse() {	
		resetSeries();
		
		// Iterate through each series
		for(int i=0; i<sampleSize; i++) {
			lineup1.resetWonDecks();
			lineup2.resetWonDecks();
			
			// Iterate through each game in the series
			while(!checkWinner()) {
								
				Deck deck1 = lineup1.selectRandomDeck();
				Deck deck2 = lineup2.selectRandomDeck();
				double matchupWinrate = winrates[deck1.getDeckID()][deck2.getDeckID()];
				double generatedRandomPercentage = Math.random();
				// Lineup1 wins the game
				if(generatedRandomPercentage < matchupWinrate) {
					deck1.setWon(true);
				}
				// Lineup2 wins the game
				else {
					deck2.setWon(true);
				}
			}
		}
		
		int totalLineup1Wins = 0;
		for (boolean element : wins) {
			if(element) {
				totalLineup1Wins++;
			}
		}
		return ((double)totalLineup1Wins)/((double)sampleSize);
	}
	
	/*
	 * Check winner, if there is record the result.
	 */
	public boolean checkWinner() {
		if(lineup1.checkSeriesWon()) {
			wins[seriesNumber] = true;
			seriesNumber++;
			return true;
		}
		if(lineup2.checkSeriesWon()) {
			wins[seriesNumber] = false;
			seriesNumber++;
			return true;
		}
		return false;
	}
	
	/*
	 * Reset series number and all the results.
	 */
	public void resetSeries() {
		seriesNumber = 0;
		for(boolean element: wins) {
			element = false;
		}
	}
	
	/*
	 * Each lineup bans its opponent's best deck against them.
	 */
	public void banDecks() {
		int[] decks1 = new int[4];
		int[] decks2 = new int[4];
		double[] wr1 = new double[4];
		double[] wr2 = new double[4];
		for(int i=0; i<4; i++) {
			decks1[i] = lineup1.getDecks()[i].getDeckID();
			decks2[i] = lineup2.getDecks()[i].getDeckID();
		}
		for(int i=0; i<4; i++) {
			wr1[i] = winrates[decks1[i]][decks2[0]] + winrates[decks1[i]][decks2[1]] + winrates[decks1[i]][decks2[2]] + winrates[decks1[i]][decks2[3]];
			wr2[i] = 1/winrates[decks1[0]][decks2[i]] + 1/winrates[decks1[1]][decks2[i]] + 1/winrates[decks1[2]][decks2[i]] + 1/winrates[decks1[3]][decks2[i]];
		}
		double max = 0;
		int index = 0;
		for(int i=0; i<4; i++) {
			if(wr1[i] > max) {
				max = wr1[i];
				index = i;
			}
		}
		lineup1.ban(index);
		
		max = 0;
		index = 0;
		for(int i=0; i<4; i++) {
			if(wr2[i] > max) {
				max = wr2[i];
				index = i;
			}
		}
		lineup2.ban(index);
	}

}

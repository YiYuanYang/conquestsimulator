public class Lineup {

	private Deck[] decks;
	private Deck[] decksAfterBan;
	private double wrAgainstField;
	
	Lineup(Deck[] decks) {
		this.decks = decks;
		this.decksAfterBan = new Deck[3];
	}
	
	/*
	 * Ban a deck and put the lineup with a banned deck into decksAfterBan
	 */
	public void ban(int bannedDeck) {
		int index = 0;
		for(int i=0; i<4; i++) {
		    if(i == bannedDeck) {
		    	continue;
		    }
		    decksAfterBan[index] = decks[i];
		    index++;
		}
	}
	
	/*
	 * Resets all decks that have won back to default state, false.
	 */
	public void resetWonDecks() {
		for (Deck element : decksAfterBan) {
		    element.setWon(false);
		}
	}
	
	/*
	 * Checks if all decks are won.
	 */
	public boolean checkSeriesWon() {
		for (Deck element : decksAfterBan) {
		    if(!element.isWon()) {
		    	return false;
		    }
		}
		return true;
	}
	
	/*
	 * Select a deck that hasn't won yet
	 */
	public Deck selectRandomDeck() {
		boolean correctDeckSelected = false;
		int selectedDeck = 0;
		while(!correctDeckSelected) {
			selectedDeck = generateRandomInteger(0,2);
			if(!decksAfterBan[selectedDeck].isWon()) {
				correctDeckSelected = true;
			}
		}
		return decksAfterBan[selectedDeck];
	}
	
	/*
	 * Selects the best deck
	 */
	public Deck selectBestDeck(double[] wr) {
		double max = 0;
		int index = 0;
		for(int i=0; i<3; i++) {
			if(decksAfterBan[i].isWon()) {
				continue;
			}
			if(wr[i] > max) {
				max = wr[i];
				index = i;
			}
		}
		return decksAfterBan[index];
	}
	
	public static int generateRandomInteger(int min, int max) {
		return (int) ((Math.random() * ((max - min) + 1)) + min);
	}

	public Deck[] getDecks() {
		return decks;
	}

	public double getWrAgainstField() {
		return wrAgainstField;
	}

	public void setWrAgainstField(double wrAgainstField) {
		this.wrAgainstField = wrAgainstField;
	}
	
}
